# Desafio Luizalabs

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/b94f0d124fe22706006e)

[![Conventional Commits](https://img.shields.io/badge/Conventional%20Commits-1.0.0-yellow.svg)](https://conventionalcommits.org)

![Coverage](/coverage.svg)


## TOC ##

- [Sobre o projeto](#sobre)
- [Pré-requisitos](#prereqs)
- [Arquitetura e Modelagem](#arquitetura)
- [Configurar](#configurar)
- [Instalar e usar](#instalar)

<a name="sobre"></a>
## Sobre o projeto ##

Desafio do luizalabs, com um contexto de clientes que possuem listas de favoritos. Esse projeto contempla tanto a busca, criação, atualização e deleteção de clientes, quanto de produtos favoritos dos clientes.


<a name="prereqs"></a>
## Pré-requisitos ##

* Python 3.6.5
* Docker version 18.09.0+
* Docker Compose 1.23.2+

<a name="arquitetura"></a>
## Arquitetura e Modelagem ##

Para esse projeto, foi utilizado o framework flask, que tem uma abordagem mais simples e se baseia na biblioteca WSGI Werkzeug. A estrutura do projeto, separa por resources, ajudando a ter um certo nível de desacoplamento, onde cada arquivo tem sua responsabilidade, como por exemplo:

```
| - app
    | - v1
        | - customers
            | - models.py
            | - routes.py
            | - schemas.py
            | - services.py
```

### Testes

Foram realizados testes unitários no projeto, utilizando `pytest`, a cobertura está proximo de `99%`. (O comando `make test-report` mostra o total de cobertura)

### Banco de dados

Duas entidades foram necessárias para esse projeto, uma forte e outra fraca.

Customer:
- id (PK)
- name
- email

CustomerProduct
- customer_id (FK)
- customer_product

### Segurança

Foi implementado uma autenticação via token JWT, utilizando o `flask-jwt`, por questões de praticidade, foi criado um usuário hardcode. Mais a frente, você pode conferir como conseguir um token.

Sabemos que o ideal é existir uma aplicação separada em um servidor separado para realizar o controle de autenticação/autorização. Mas para esse projeto foi implementado aqui mesmo.

### Cache

Também foi implementado um sistema de cache, tanto na API externa de produtos, quanto nos endpoints desse projeto, que utilizam paginação. Para API externa foi utilizado a lib `requests-cache` e para os endpoints internos a lib `flask-cache`.


<a name="configurar"></a>
## Configurar ##

### Variaveis de embiente:

Para utilizar os comandos descritos no `Makefile`, é necessário criar um arquivo `.env`. Você pode, também, criar um arquivo de configuração para cada ambiente: `.env.dev`, `.env.staging` e `.env.prod`. Configure de acordo com a tabela abaixo e o exemplo `env.example` fornecido:

Configure as variáveis de ambiente de acordo com cada ambiente:

| Nome | Descrição | Valor Padrão | Obrigatório | Apenas ambiente de dev
| -- | -- | -- | -- | -- |
| FLASK_APP | Define o módulo ou pacote onde a aplicação está | server | :white_check_mark: | Não
| FLASK_RUN_HOST | Define o hostname da aplicação |  localhost | :white_check_mark: | Sim
| FLASK_RUN_PORT | Define a porta que a aplicação vai usar |  5000 | :white_check_mark: | Sim
| FLASK_ENV | Descreve o ambiente que está sendo utilizado (development, staging ou production) |  development | :white_check_mark: | Não
| FLASK_CONFIG | Indica quais configurações devem ser utilizadas no arquivo `config.py` | development | :white_check_mark: | Não
| DB_NAME | Parâmetro usado para setar o nome do database do banco de dados|  Vazio | :white_check_mark: |
| DB_USER | Parâmetro usado para setar o usuário que fará acesso ao banco de dados do banco|  Consulte | :white_check_mark: |
| DB_PASSWORD | Parâmetro usado para setar a senha do usuário que fará acesso ao banco de dados| Consulte | :white_check_mark: |
| DB_HOST | Parâmetro usado para setar o IP ou hostname do banco de dados |  Consulte | :white_check_mark: |
| DB_PORT | Parâmetro usado para setar a porta do banco de dados | 3306 | :white_check_mark: |
| DATABASE_URL | Parâmetro usado para setar a url do banco de dados, é a junção das variaveis de DB acima | N/A | :white_check_mark: | Não |
| LOG_LEVEL | Parâmetro usado para setar qual o nivel de log da aplicação | N/A | :x: | Não |
| CACHE_CONFIG | Parâmetro usado para setar a config do flask_caching | N/A | :x: | Não |
| SECRET_KEY | Parâmetro usado para assinar o algoritmo do jwt  | N/A | :x: | Não |
| JWT_ACCESS_TOKEN_EXPIRES | Parâmetro usado para setar o tempo de expiração do token jwt  | N/A | :x: | Não |


Para alternar entre os ambientes de **dev**, **staging** e **prod**, utilize um dos comandos abaixo:

```bash
make env-switch-dev
make env-switch-staging
make env-switch-prod
```

**Obs. 4**: o que os comandos acima fazem é copiar o conteúdo do arquivo em questão, por exemplo `.env.dev` para `env-switch-dev`, e colar no arquivo `.env`.

### Descrição dos commandos disponiveis 

### `make install`

Instala as dependências do projeto.

```bash
make install
```

A mensagem abaixo indicará que as bibliotecas foram instaladas com sucesso:

```bash
Limpando arquivos temporários... OK
Instalando 'pip' e 'virtualenv'... OK
Destruindo ambiente virtual... OK
Criando ambiente virtual... OK
Instalando bibliotecas... OK
Inicializando banco de dados...
Tabelas carregadas - OK
============================================
Tudo pronto para o desenvolvimento

Digite para ativar o ambiente:

source venv/bin/activate
============================================

```

Neste ponto, foi disponibilizado um ambiente todo operacional, com o container de banco de dados e uma base já com dados e pronta para operar. Ative o ambiente virtual para começar a trabalhar:

```bash
source venv/bin/activate
```

### `make debug`

Inicia a aplicação localmente em modo de desenvolvimento, refletindo alterações em tempo de execução.

### `make run`

Inicia a aplicação com o web server Gunicorn (deve ser utilizado em ambientes sérios, como homologação e produção).

### `make test`

Executa testes unitários.

### `make docker-up`

Inicia a infraestrutura (container MySQL, etc.) necessária para a API executar localmente.

### `make docker-down`

Desliga os containers de infraestrutura da aplicação.

### `make db-upgrade`

Quando você fizer alterações nos modelos (de banco de dados), execute este comando para aplicá-las versionar e aplicá-las no banco de dados.

### `make db-downgrade`

Desfaz as alterações feitas no banco de dados, execute este script.



<a name="instalar"></a>
## Instalar e Usar ##

Para iniciar o projeto, rode os comandos abaixo:

- Crie um arquivo `.env` na raiz do projeto, para isso, você pode usar o comando:

```
touch .env
```
- use as variaveis de ambiente de dev (foram versionadas de proposito, para facilitar testar a aplicação), com o comando:
```
make env-switch-dev
```
- Instale as dependencias, com o comando:
```
make install
```
- Entre no venv com o comando:
```
source venv/bin/activate
```
- Inicie o banco de dados e construa a tabelas com o comando:
```
make db-init
```
- Finalmente, inicie a aplicação
```
make debug
```

Ao acessar no browser a URL que está rodando a aplicação (`http://localhost:5000`), você conseguirá visualizar o `swagger`, especificando cada método da aplicação.

Pronto a aplicação já está de pé, e você pode fazer requisições para os endpoints. Mas para isso, você deve se autenticar:

### Autenticação

A API é protegida através de tokens jwt, para conseguir um token, faça uma requisição `POST` para a rota `http://localhost:5000/auth`, com o seguinte objeto no `body`:

```
{
    "username": "luizalabs",
    "password": "luizalabs$1"
}
```

* Foi optado por ser criado um usuário hardcode por questões de praticidade

Você irá receber um `access_token`, semelhante a esse:

```
{
  "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1ODMzNzA3MTcsImlhdCI6MTU4MzM3MDQxNywibmJmIjoxNTgzMzcwNDE3LCJpZGVudGl0eSI6MX0.DMcLHKdq74OOY1wzz993MN8Qf-cY7KefpIpF-l7YK4s"
}
```

Utilize esse token, nas demais requisições da API, como por exemplo:

```
curl --location --request GET 'http://localhost:5000/v1/customers' \
--header 'Authorization: jwt eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1ODMzNzEwODUsImlhdCI6MTU4MzM3MDc4NSwibmJmIjoxNTgzMzcwNzg1LCJpZGVudGl0eSI6MX0.Z_dtVg3ucRoM1t0hivrkD3-DI5uwV4yaUk7-XBNKnHY'
```

Você vai receber um retorno igual a esse:

```
    {
    "data": [
        {
            "id": 1,
            "name": "Paola Carosella",
            "email": "paola.carosella@masterchef.com"
        },
        {
            "id": 2,
            "name": "Henrique Foga\u00e7a",
            "email": "henrique.fogaca@masterchef.com"
        },
        {
            "id": 3,
            "name": "Érick Jacquin",
            "email": "erick.jacquin@masterchef.com"
        }
    ],
    "meta": {
        "limit": 20,
        "offset": 1,
        "total": 3
    }
}
```

Você pode utilizar o link do postman que está no topo desse documento para realizar as chamadas.

É possivel rodar os testes com o comando:
```
make test
```
E verificar o relatório de cobertura, com o comando:
```
make test-report
```