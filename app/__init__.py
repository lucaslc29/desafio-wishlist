import os
import logging
import requests_cache

from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from app.contrib.errors import register_custom_error_handler
from app.contrib.auth import authenticate, identity
from flask_jwt import JWT
from app.config import config
from flask_caching import Cache

db = SQLAlchemy()
cache = Cache(config=os.getenv('CACHE_CONFIG', {"CACHE_TYPE": "simple"}))

def create_app(config_name):
    # :: configura qual o nivel de logs vamos querer
    log_level = config[config_name].LOG_LEVEL
    logging.basicConfig(level=log_level)
    logging.debug(f'LOG_LEVEL da aplicação configurado para {log_level}')

    # :: configurações sobre o app, como: ambiente, contexto, banco e cors
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    app.url_map.strict_slashes = False
    app.app_context().push()
    logging.debug('Iniciando o app ...')
    db.init_app(app)
    logging.debug('Iniciando conexão com o banco de dados ...')
    Migrate(app, db)
    logging.debug('Fazendo o seed no banco ...')
    CORS(app)
    
    # Registra todos os blueprints
    from .core import api as core
    app.register_blueprint(core.blueprint)
    
    from .v1 import api as v1
    app.register_blueprint(v1.blueprint, url_prefix='/v1')

    # :: configura o cache interno
    cache.init_app(app)

    # :: configura o cache das chamdas externas
    requests_cache.install_cache(cache_name='luizalabs_cache', expire_after=180)

    # :: configura o handler de erro customizado
    register_custom_error_handler(app)

    # :: configura a authenticação
    app.config['SECRET_KEY'] = config[config_name].SECRET_KEY
    JWT(app, authenticate, identity)

    # :: configura a validade dos tokens
    app.config['JWT_ACCESS_TOKEN_EXPIRES'] = config[config_name].JWT_ACCESS_TOKEN_EXPIRES

    logging.debug('Aplicação está pronta para rodar ...')

    return app
