"""
    Implementação do token JWT
"""

from werkzeug.security import safe_str_cmp


# :: Preferi utilizar um usuário hardcode do que criar uma estrutura mais elaborada.
class User(object):
    def __init__(self, id, username, password):
        self.id = id
        self.username = username
        self.password = password

    def __str__(self):
        return "User(id='%s')" % self.id

users = [
    User(1, 'luizalabs', 'luizalabs$1')
]

username_table = {u.username: u for u in users}
userid_table = {u.id: u for u in users}

# :: Método chamado para authenticar o usuário, verifica a senha e retorna o user
def authenticate(username, password):
    user = username_table.get(username, None)
    if user and safe_str_cmp(user.password.encode('utf-8'), password.encode('utf-8')):
        return user

# :: Retorna a identidade atual
def identity(payload):
    user_id = payload['identity']
    return userid_table.get(user_id, None)