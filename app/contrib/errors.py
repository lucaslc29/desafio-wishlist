import os

from flask import jsonify

_error_list = {
    # Erros gerais
    500: "Internal Server Error",
    # Erros de entidade
    1000: "Resource not found",
    # Erros da autenticação
    2000: "Authentication Server Unavailable",
    2001: "Unauthorized",
    2002: "Invalid Authorization Header"
}

class CustomError(Exception):

    def __init__(self, status=500, code=500, message=None):
        super().__init__(self)
        self.status = status
        self.code = code
        self.message = message
       
    def to_json(self):
        error_json = {
            'status': self.status,
            'code': self.code,
            'message': self.message if self.message else _error_list.get(self.code)
        }

        return error_json

# :: Erros customizados
NOT_FOUND = CustomError(404, 1000, "Você está buscando um registro que não existe")
PRODUCT_NOT_FOUND = CustomError(404, 1001, "Esse produto não existe")
SSO_UNAVAILABLE = CustomError(503, 2000)
UNAUTHORIZED = CustomError(403, 2001)
INVALID_AUTHORIZATION = CustomError(401, 2002)
DUPLICATE_ENTRY = CustomError(409, 1062, "Você está tentando inserir um registro que já existe")
INVALID_PARAMETERS = CustomError(400, 1063, "Parametros inválidos")

def handle_custom_error(error):
    return jsonify(error.to_json()), error.status

def handle_any_error_as_custom_error(error, status=500):
    return jsonify(CustomError(message=str(error)).to_json()), status

def register_custom_error_handler(app):
    app.register_error_handler(CustomError, handle_custom_error)
