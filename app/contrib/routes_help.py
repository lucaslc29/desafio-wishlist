from flask import url_for
import os
import requests

from datetime import datetime
from functools import wraps, partial
from distutils.util import strtobool

from flask import request
from flask_restx.reqparse import Argument, RequestParser


def create_page_result(method_endpoint, pagination, args, transformed_items=None, **values):
    """
    Função para auxiliar na criação do schema de devolução de um endpoint que contempla paginação
    """
    prev, next = None, None

    if pagination.has_prev:
        prev = url_for(method_endpoint, _external=True, limit=pagination.per_page,\
            offset=pagination.prev_num, order=args['order'], **values)
    if pagination.has_next:
        next = url_for(method_endpoint, _external=True, limit=pagination.per_page,\
            offset=pagination.next_num, order=args['order'], **values)

    data = transformed_items if transformed_items != None else pagination.items
    
    return {
        "data": data,
        "meta": {
            "prev": prev,
            "next": next,
            "limit": args['limit'],
            "offset": args['offset'],
            "order": args['order'],
            "total": pagination.total
        }
    }

def parse_params(*arguments):
    """
    Parser de parametros
    """
    def decorated(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            parser = RequestParser()
            for argument in arguments:
                parser.add_argument(argument)
            
            previous_args = kwargs.get('args', {})
            updated_args = { **request.args.to_dict(), **previous_args, **parser.parse_args() }
            kwargs.update(args=updated_args)

            return func(*args, **kwargs)
        return wrapper
    return decorated

def paginate(func):
    """
    Parâmetros padrão para paginação
    """
    @wraps(func)
    @parse_params(
            Argument('limit', 20, type=int, location='args'),
            Argument('offset', 1, type=int, location='args'),
            Argument('order', type=str, location='args')
        )
    def decorated(*args, **kwargs):
        return func(*args, **kwargs)
    return decorated
