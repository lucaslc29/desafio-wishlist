
import os
import requests
import logging
from app.contrib.errors import PRODUCT_NOT_FOUND
class ChallengeHttpClient():

    URL = os.getenv('APIPRODUCTS_URL')
    logging.debug(f'URL da API Externa de Produtos: {URL}')

    def get_product(self, id):
        resp = requests.get(f'{self.URL}/product/{id}')
        if resp.status_code == 404:
                raise PRODUCT_NOT_FOUND
        return resp
