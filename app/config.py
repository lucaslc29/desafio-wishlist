import os
import datetime
import logging 
basedir = os.path.abspath(os.path.dirname(__file__))

# :: Realiza a configuração necessário de acordo com o ambiente que estiver rodando
class Config:
    STATIC_FOLDER = 'static'
    TESTING = False

    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = os.getenv('DATABASE_URL')

    LOG_LEVEL = os.getenv('LOG_LEVEL', 'INFO')
    SECRET_KEY = os.getenv('SECRET_KEY', 'super-secret')
    JWT_ACCESS_TOKEN_EXPIRES = os.getenv('JWT_ACCESS_TOKEN_EXPIRES', datetime.timedelta(weeks=2))
class DevelopmentConfig(Config):
    logging.debug('Configurações de desenvolvimento iniciadas ...')
    DEBUG = True

    optional_server_name = f"{os.getenv('FLASK_RUN_HOST', '127.0.0.1')}:{os.getenv('FLASK_RUN_PORT', 5000)}"
    SERVER_NAME = os.getenv('SERVER_NAME', optional_server_name)

class TestingConfig(Config):
    logging.debug('Configurações de teste iniciadas ...')
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite://'
    PRESERVE_CONTEXT_ON_EXCEPTION = False

    optional_server_name = f"{os.getenv('FLASK_RUN_HOST', '127.0.0.1')}:{os.getenv('FLASK_RUN_PORT', 5000)}"
    SERVER_NAME = os.getenv('SERVER_NAME', optional_server_name)

class StagingConfig(Config):
    logging.debug('Configurações de staging iniciadas ...')
    DEBUG = True

class ProductionConfig(Config):
    logging.debug('Configurações de produção iniciadas ...')
    DEBUG = False

config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'staging': StagingConfig,
    'production': ProductionConfig,
    'default': DevelopmentConfig
}
