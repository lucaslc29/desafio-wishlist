from flask_restx import Api
from flask import Blueprint
from app.core.routes import api as core_ns
from app.contrib.errors import handle_any_error_as_custom_error

blueprint = Blueprint('core', __name__)

api = Api(blueprint, title = 'API Core', version = 'v1')

# :: Seleciona o handler de erro padrão
api.errorhandler(handle_any_error_as_custom_error)

# :: Carrega as rotas do core
api.add_namespace(core_ns, path='/')
