import os

from flask_restx import fields

SERVER_NAME = os.getenv('SERVER_NAME')

# :: Schemas utilizados quando paginamos uma requisição

def _page(api, model):
    return api.model('Page', {
        'data': fields.List(fields.Nested(model, skip_none=True)),
        'meta': fields.Nested(_meta(api), skip_none=True)
    })

def _meta(api):
    return api.model('Meta', {
        'prev': fields.String(description = 'URL to prev page'),
        'next': fields.String(description = 'URL to next page'),
        'limit':fields.Integer(description='Limit per page'),
        'offset':fields.Integer(description='Number of pages'),
        'order': fields.String(description = 'Order of results'),
        'total': fields.Integer(description = 'Total number of results'),
        'server': fields.String(default=SERVER_NAME)
    })  
