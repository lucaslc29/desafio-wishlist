from sqlalchemy import asc, desc, exc
from app.contrib.errors import NOT_FOUND, DUPLICATE_ENTRY, INVALID_PARAMETERS
from app import db

class Service(object):
    """
    Operações básicas que são reutilizadas em todos os endpoints
    """
    __model__ = None

    def _isinstance(self, model, raise_error=True):
        rv = isinstance(model, self.__model__)
        if not rv and raise_error:
            raise ValueError(f'{model} is not of type {self.__model__}')
        return rv

    def _preprocess_params(self, kwargs):
        return kwargs

    def save(self, model):        
        self._isinstance(model)
        db.session.add(model)
        db.session.commit()
        return model

    def all(self):
        return self.__model__.query.all()
   
    def page(self, args, return_query=False, use_like=True, like_exceptions=[]):
        order = args.get('order', None)
        sort = desc(order[1:]) if order and '-' in order else asc(order)

        query = db.session.query(self.__model__).order_by(sort)
        
        # ignora todos os parametros vazios que foram enviados
        not_empty_args = {k:v for (k,v) in args.items() if v != ''}
        for k, v in not_empty_args.items():
            if hasattr(self.__model__, k):
                field = getattr(self.__model__, k)
                if (use_like and k not in like_exceptions):
                    query = query.filter(field.ilike(f'%{v}%'))
                else:
                    query = query.filter(field.ilike(v)) #  usando ilike para mater o case sensitive

        if return_query:
            return query
        
        return query.paginate(args.get('offset', 1), args.get('limit', 20), False)
           
    def get(self, id):
        return self.__model__.query.get(id)

    def get_all(self, *ids):
        return self.__model__.query.filter(self.__model__.id.in_(ids)).all()

    def find(self, **kwargs):
        return self.__model__.query.filter_by(**kwargs)

    def first(self, **kwargs):
        return self.find(**kwargs).first()

    def get_or_404(self, id):
        model = self.get(id)
        if not model:
            raise NOT_FOUND
        return model

    def new(self, **kwargs):
        return self.__model__(**self._preprocess_params(kwargs))

    def create(self, **kwargs):
        try:
            return self.save(self.new(**kwargs))
        except exc.IntegrityError as ex:
            if ex.code == 'gkpj':
                raise DUPLICATE_ENTRY
        except exc.InvalidRequestError as ex:
            if ex.code == '7s2a':
                raise INVALID_PARAMETERS
        finally:
            db.session.rollback()
            db.session.flush()

    def update(self, model, **kwargs):
        self._isinstance(model)
        for k, v in self._preprocess_params(kwargs).items():
            setattr(model, k, v)
        self.save(model)
        return model

    def delete(self, model):
        try:
            self._isinstance(model)
            db.session.delete(model)
            db.session.commit()
        except Exception as ex:
            raise ex
        finally:
            db.session.close()
