from datetime import datetime
from flask import redirect
from flask_restx import Namespace, Resource

api = Namespace('default', description='Default utility operations')

start_time = datetime.now()

@api.route('/')
class Index(Resource):
    
    def get(self):
        """
        Redireciona para o rota /v1 que é a padrão
        """
        return redirect('/v1')


@api.route('health')
class Healthcheck(Resource):

    @api.doc('Health Checker')
    def get(self):
        """
        Mensagem de Healthcheck
        """
        return { 'message': 'I\'m alive and dancing smooth criminal!' }
