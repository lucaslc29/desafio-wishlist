from flask import Blueprint
from flask_restx import Api

blueprint = Blueprint('v1', __name__)

api = Api(
        blueprint,
        title = 'API',
        version = 'v1',
        description = 'API de Wishlist dos Clientes Magazine Luiza'
        )

# :: Carrega todas as rotas
from .customers.routes import api as customers_ns, route_name as customers_path
api.add_namespace(customers_ns, path=f'/{customers_path}')

