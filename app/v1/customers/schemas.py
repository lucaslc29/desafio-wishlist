"""
    Schema de clientes
"""

from flask_restx import fields
from app.core.schemas import _page
from app.v1 import api

customer = api.model('customer', {
    'id': fields.Integer(readOnly=True),
    'name': fields.String(required=True),
    'email': fields.String(requited=True, unique=True)
})

page = _page(api, customer)
