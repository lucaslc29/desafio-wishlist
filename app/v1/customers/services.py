from app.core.services import Service
from app.v1.customers.models import Customer
from app.v1.customers_products.models import CustomerProduct

class CustomerService(Service):

    __model__ = Customer
    