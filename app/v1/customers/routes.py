from flask_restx import Namespace, Resource
from app.contrib.routes_help import create_page_result, paginate, parse_params, Argument
from app.v1.customers.services import CustomerService
from app.v1.customers_products.services import CustomersProductsService
from app.v1.customers.schemas import customer, page
from app.v1.customers_products.schemas import products_page, product, customers_products
from flask_jwt import jwt_required, current_identity
from app import cache

route_name = __name__.split('.')[2]
api = Namespace(route_name, description = 'Customer operations')

# :: Instancia do service utilizado nessa rota
service = CustomerService()
customers_products_service = CustomersProductsService()

# :: Argumentos para serem validados nas rotas
NAME_ARG = Argument('name', type=str, location='json', required=True)
EMAIL_ARG = Argument('email', type=str, location='json', required=True)

@api.route('/')
class CustomerList(Resource):
    
    @api.doc(f'list_of_{route_name}')
    @api.marshal_with(page)
    @paginate
    @jwt_required()
    @cache.cached(timeout=50)
    def get(self, args):
        """
        Lista todos os clientes
        """
        pagination = service.page(args)
        return create_page_result('v1.customers_customer_list', pagination, args)

    @api.doc(f'create_{route_name}')
    @api.expect(customer, validate=True)
    @api.marshal_with(customer, skip_none=True)
    @parse_params(NAME_ARG, EMAIL_ARG)
    @jwt_required()
    def post(self, args):
        """
        Cria um novo cliente
        """
        return service.create(**api.payload), 201

@api.route ('/<int:id>')
@api.doc(params={'id': 'customer id'})
class CustomerRoute(Resource):

    @api.doc(f'get_{route_name}')
    @api.marshal_with(customer, skip_none=True)
    @jwt_required()
    def get(self, id):
        """
        Busca um cliente pelo ID
        """
        return service.get_or_404(id)
        
    @api.doc(f'update_{route_name}')
    @api.expect(customer)
    @api.marshal_with(customer, skip_none=True)
    @jwt_required()
    def put(self, id):
        """
        Atualiza um cliente existente
        """
        customer = service.get_or_404(id)
        return service.update(customer, **api.payload)

    @api.doc(f'delete_{route_name}')
    @jwt_required()
    def delete(self, id):
        """
        Remove um cliente pelo ID
        """
        customer = service.get_or_404(id)
        service.delete(customer)
        return '', 204

@api.route ('/<int:id>/wishlist')
@api.doc(params={'id': 'customer id'})
class CustomerWishlist(Resource):

    @api.doc(f'get_wishlist_of_{route_name}')
    @api.marshal_with(products_page, skip_none=False)
    @paginate
    @jwt_required()
    @cache.cached(timeout=50)
    def get(self, args, id):
        """
        Busca a wishlist de um cliente
        """
        args['customer_id'] = id
        pagination = customers_products_service.page(args)

        return create_page_result('v1.customers_customer_wishlist', pagination, args)

    @api.doc(f'create_{route_name}')
    @api.expect(customers_products, validate=True)
    @api.marshal_with(product, skip_none=True)
    @jwt_required()
    def post(self, id):
        """
        Adiciona um produto a wishlist de um cliente
        """
        api.payload['customer_id'] = id
        return customers_products_service.create(**api.payload), 201

@api.route ('/<int:id>/wishlist/<string:product_id>')
@api.doc(params={'id': 'customer id'})
@api.doc(params={'product_id': 'product id'})
class CustomerWishlistResource(Resource):

    @api.doc(f'get_{route_name}')
    @api.marshal_with(product, skip_none=True)
    @jwt_required()
    def get(self, id, product_id):
        """
        Retorna um produto especifico da wishlist
        """
        return customers_products_service.get_or_404(id, product_id)

    @api.doc(f'delete_{route_name}')
    @jwt_required()
    def delete(self, id, product_id):
        """
        Remove um produto da wishlist de um cliente
        """
        product = customers_products_service.first(customer_id=id, product_id=product_id)
        customers_products_service.delete(product)
        return '', 204
