"""
    Modelo de Clientes
"""

from app import db

class Customer(db.Model):

    __tablename__ = 'customers'
    
    id = db.Column(db.Integer, primary_key=True, autoincrement=True, index=True)
    name = db.Column(db.String(200), nullable=False)
    email = db.Column(db.String(200), nullable=False, unique=True, index=True)

    customers_products = db.relationship("CustomerProduct", cascade="all,delete")