"""
    Schemas do Cliente-Produto e de apenas Produtos também
"""

from flask_restx import fields
from app.core.schemas import _page
from app.v1 import api

customers_products = api.model('customers_products', {
    'customer_id': fields.Integer(),
    'product_id': fields.String(required=True)
})

customers_products_page = _page(api, customers_products)

product = api.model('product', {
    'id': fields.String(readOnly=True),
    'title': fields.String(readOnly=True),
    'image': fields.String(readOnly=True),
    'price': fields.Float(readOnly=True),
    'review_score': fields.Float(readOnly=True, attribute='reviewScore')
})

products_page = _page(api, product)