"""
    Model Cliente-Produto
"""

from app import db
from sqlalchemy import ForeignKey

class CustomerProduct(db.Model):

    __tablename__ = 'customers_products'
    
    customer_id = db.Column(db.Integer, ForeignKey('customers.id'), primary_key=True, index=True)
    product_id = db.Column(db.String(50), primary_key=True)
