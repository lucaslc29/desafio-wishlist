import os
import requests
from app.core.services import Service
from app.v1.customers_products.models import CustomerProduct
from app.contrib.errors import NOT_FOUND
from app.contrib.http_clients import ChallengeHttpClient

class CustomersProductsService(Service):

    __model__ = CustomerProduct
    challenge_client = ChallengeHttpClient()

    def page(self, args):
        query = super().page(args)
        result = []

        for item in query.items:
            response = self.challenge_client.get_product(item.product_id)
            data = response.json()

            result.append(data)

        query.items = result
        return query

    def first(self, customer_id, product_id):
        model = super().first(customer_id=customer_id, product_id=product_id)

        if not model:
            raise NOT_FOUND

        return model
    
    def get_or_404(self, customer_id, product_id):
        model = super().first(customer_id=customer_id, product_id=product_id)
        
        if not model:
            raise NOT_FOUND

        resp =  self.challenge_client.get_product(product_id)

        return resp.json()
    
    # :: Método overrride para validar o produto
    def _preprocess_params(self, kwargs):
        product_id = kwargs.get('product_id', None)
        if product_id:
            resp =  self.challenge_client.get_product(product_id)
        return kwargs

    # :: Método override para conseguir retornar o payload de um produto 
    def create(self, **kwargs):
        super().create(**kwargs)
        product_id = kwargs.get('product_id')
        resp =  self.challenge_client.get_product(product_id)
        return resp.json()