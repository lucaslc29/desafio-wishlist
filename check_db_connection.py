import os
import pymysql

DB_HOST = os.getenv('DB_HOST')
DB_PORT = int(os.getenv('DB_PORT'))
DB_NAME = os.getenv('DB_NAME')
DB_USER = os.getenv('DB_USER')
DB_PASSWORD = os.getenv('DB_PASSWORD')

try:
    print('Iniciando conexão com o banco de dados...')
    pymysql.connect(host=DB_HOST, port=DB_PORT, db=DB_NAME, user=DB_USER, password=DB_PASSWORD)
except Exception as ex:
    print('Não foi possível conectar ao banco de dados. ERRO:', ex)
    exit(1)
