-r requirements.txt 
pytest==5.2.1
pytest-flask==0.15.1
coverage==5.0.3
coverage-badge==1.0.1