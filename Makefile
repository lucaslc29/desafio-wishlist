include .env
export $(shell sed 's/=.*//' .env)

env-create: env-destroy
	@printf "Criando ambiente virtual... "
	@virtualenv -q -p python3.6 venv
	@echo "$(SUCCESS)"

env-destroy:
	@printf "Destruindo ambiente virtual... "
	@rm -rfd venv
	@rm -rfd migrations
	@echo "$(SUCCESS)"

env-switch-dev:
	@echo "switching to DEV..."
	@cp .env.dev .env

env-switch-staging:
	@echo "switching to Staging..."
	@cp .env.staging .env

env-switch-prod:
	@echo "switching to Prod..."
	@cp .env.prod .env

docker-up:
	@docker-compose --log-level ERROR up -d
	
docker-down:
	@docker-compose down

clear:
	@printf "Limpando arquivos temporários... "
	@rm -f dist/*.gz
	@rm -rfd *.egg-info
	@find . -type f -name '*.pyc' -delete
	@find . -type f -name '*.log' -delete
	@echo "$(SUCCESS)"

system-packages:
	@printf "Instalando 'pip' e 'virtualenv'... "
	@curl -s https://bootstrap.pypa.io/get-pip.py -o get-pip.py
	@python3  -q  get-pip.py 1> /dev/null --user
	@pip install -q -U pip
	@pip install -q virtualenv --user
	@rm get-pip.py
	@echo "$(SUCCESS)"

packages: env-create
	@printf "Instalando bibliotecas... "
	@venv/bin/pip install -q --no-cache-dir -r requirements-dev.txt
	@echo "$(SUCCESS)"

db-init:
	@echo "Inicializando banco de dados... "
	@docker-compose --log-level ERROR up -d
	@while ! venv/bin/python check_db_connection.py; do sleep 1; done;
	@echo "Banco de dados inicializado - $(SUCCESS)"
	@echo "Carregando tabelas... "
	@rm -rfd migrations
	@venv/bin/flask db init 1> /dev/null
	@venv/bin/flask db migrate -m initial_version 1> /dev/null
	@venv/bin/flask db upgrade 1> /dev/null
	@venv/bin/flask db stamp heads 1> /dev/null
	@venv/bin/flask load
	@echo "Tabelas carregadas - $(SUCCESS)"

db-downgrade:
	@venv/bin/flask db downgrade

db-upgrade:
	@venv/bin/flask load
	@venv/bin/flask db stamp heads
	@venv/bin/flask db migrate
	@venv/bin/flask db upgrade

test:
	@coverage run --source=./app -m pytest -x -v -s

test-report:
	@coverage report -m
	@rm coverage.svg
	@coverage-badge -o coverage.svg

install: clear system-packages packages
	@echo "============================================"
	@echo "Tudo pronto para começar o desenvolvimento"
	@echo ""
	@echo "Digite para ativar o ambiente: "
	@echo ""
	@echo "source venv/bin/activate"
	@echo "============================================"

debug:
	@venv/bin/flask run

run:
	@venv/bin/gunicorn -w 1 -b 0.0.0.0:5000 server:app

