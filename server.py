import os

from app import create_app, db
from random import randint
from datetime import datetime, timedelta
from app.v1.customers.models import Customer
from app.v1.customers_products.models import CustomerProduct

CONFIG = os.getenv('FLASK_CONFIG', 'default')

app = create_app(CONFIG)

# :: Comandos relacionados ao banco de dados
@app.teardown_request
def shutdown_session(exception=None):
    db.session.remove()

@app.cli.command()
def load():
    db.create_all()

@app.cli.command()
def load():
    db.create_all()

    # :: Adiciona clientes
    db.session.add(Customer(name='Paola Carosella', email='paola.carosella@masterchef.com'))
    db.session.add(Customer(name='Henrique Fogaça', email='henrique.fogaca@masterchef.com'))
    db.session.add(Customer(name='Érick Jacquin', email='erick.jacquin@masterchef.com'))

    # :: Adiciona produtos na wishlist do primeiro cliente
    db.session.add(CustomerProduct(customer_id=1, product_id='1bf0f365-fbdd-4e21-9786-da459d78dd1f'))
    db.session.add(CustomerProduct(customer_id=1, product_id='6a512e6c-6627-d286-5d18-583558359ab6'))
    db.session.add(CustomerProduct(customer_id=1, product_id='212d0f07-8f56-0708-971c-41ee78aadf2b'))

    db.session.commit()

# :: Remove as tabelas do banco
@app.cli.command()
def unload():
    for table in reversed(db.metadata.sorted_tables):
        db.session.execute(table.delete())
    db.session.execute('drop table alembic_version')
    db.session.commit()
