"""
    Testes relacionados a wishlist do cliente
"""

import pytest
from app.contrib.errors import CustomError, NOT_FOUND

from app.v1.customers_products.services import CustomersProductsService
service = CustomersProductsService()

# :: Método get_or_404 - passando parametros inexistentes, é esperado NOT_FOUND
def test_get_or_404_wrong_parameters():
    with pytest.raises(CustomError) as ex:
        resp = service.get_or_404(1,'abc')
    assert ex._excinfo[1] == NOT_FOUND

# :: Método get_or_404 - passando parametros inválidos (que são tipos diferentes), é esperado NOT_FOUND
def test_get_or_404_invalid_parameters():
    with pytest.raises(CustomError) as ex:
        resp = service.get_or_404(1,123)
    assert ex._excinfo[1] == NOT_FOUND

# :: Método get_or_404 - passando parametros corretos e existentes, é esperado retornar o objeto
def test_get_or_404():
    resp = service.get_or_404(1, '1bf0f365-fbdd-4e21-9786-da459d78dd1f')
    assert resp['id'] == '1bf0f365-fbdd-4e21-9786-da459d78dd1f'
    assert resp['title'] == 'Cadeira para Auto Iseos Bébé Confort Earth Brown'

# :: Método page, passando parametros certos, para ver a paginação funcionando
def test_page():
    args = { 'limit': 1, 'offset': 2, 'order': None }
    resp = service.page(args)
    assert resp.per_page == args['limit']
    assert resp.page == args['offset']

# :: Método page, filtrando pelo id do cliente, é esperado o payload do primeiro produto
def test_page_filter_by_customer_id():
    args = { 'limit': 1, 'offset': 1, 'order': None, 'customer_id': 1 }
    resp = service.page(args)
    expected = {
        'price': 1699.0,
        'image': 'http://challenge-api.luizalabs.com/images/1bf0f365-fbdd-4e21-9786-da459d78dd1f.jpg',
        'brand': 'bébé confort',
        'id': '1bf0f365-fbdd-4e21-9786-da459d78dd1f',
        'title': 'Cadeira para Auto Iseos Bébé Confort Earth Brown'
    }
    assert resp.items[0] == expected

# # :: Método page, passando parametros inválidos, é esperado que apenas ignore o parametro e retorne os 3 totais
def test_page_wrong_parameters():
    args = { 'offset': 1, 'order': None, 'non_exists': 1 }
    resp = service.page(args)
    assert resp.total == 3
