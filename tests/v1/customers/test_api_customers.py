"""
    Chamadas referentes aos ENDPOINTS de clientes
"""
import json
import pytest
from app.contrib.errors import NOT_FOUND, PRODUCT_NOT_FOUND
from tests.helper import create_header_with_authorization

@pytest.fixture
def headers(client):
    return create_header_with_authorization(client)

# :: GET /customers - Busca todos os registro, paginados, é esperado que retorne normalmente
def test_get_customers(client, headers):
    response = client.get('/v1/customers', headers=headers)
    expected = {'id': 1, 'name': 'Paola Florencia Carosella', 'email': 'paola.carosella@masterchef.com'}
    assert response.status_code == 200
    assert response.json['data'][0] == expected

# :: GET /customers - Busca todos os registros passando parametros que não existem de querystring, 
# é esperado que os parametros sejam ignorados
def test_get_customers_querystring(client, headers):
    resp = client.get('/v1/customers', data={ 'test': 'test' }, headers=headers)
    expected = {'id': 1, 'name': 'Paola Florencia Carosella', 'email': 'paola.carosella@masterchef.com'}
    assert resp.status_code == 200
    assert resp.json['data'][0] == expected

# :: POST /customers - Tenta criar um cliente com parametros de tipo inválidos 
def test_post_customers_invalid_parameters_types(client, headers):
    data = {
        'name': 'cliente de teste',
        'email': 123
    }
    resp = client.post('/v1/customers', json=data, headers=headers)
    assert resp.status_code == 400
    assert resp.json['errors']['email'] == "123 is not of type 'string'"

# :: POST /customers - Tenta criar um cliente com parametros que não existem
def test_post_customer_invalid_parameters(client, headers):
    data = {
        'name': 'cliente de teste',
        'email2': 'cliente@teste.com'
    }
    resp = client.post('/v1/customers', json=data, headers=headers)
    assert resp.status_code == 400
    assert resp.json['errors']['email'] == "Missing required parameter in the JSON body"

# :: POST /customer - Insere um novo cliente
def test_post_customer_new(client, headers):
    data = {
        'name': 'Cliente de teste',
        'email': 'test@test.com'
    }
    resp = client.post('/v1/customers', json=data, headers=headers)
    assert resp.status_code == 201

# :: POST /customers - Tenta inserir um cliente que já existe
def test_post_customer_already_exists(client, headers):
    data = {
        'name': 'Paola Florencia Carosella',
        'email': 'paola.carosella@masterchef.com'
    }
    resp = client.post('/v1/customers', json=data, headers=headers)
    assert resp.status_code == 409
    assert resp.json['message'] == 'Você está tentando inserir um registro que já existe'

# :: GET /customers/<int:id> - Tenta buscar os registros de um cliente que não existe
def test_get_customer_doesnt_exists(client, headers):
    resp = client.get('/v1/customers/999', headers=headers)
    assert resp.status_code == 404
    assert resp.json['message'] == 'Você está buscando um registro que não existe'

# :: GET /customers/<int:id> - Tenta buscar os registros de um cliente, passando uma string ao inves de int
def test_get_customer_invalid_parameter(client, headers):
    resp = client.get('/v1/customers/AAA', headers=headers)
    assert resp.status_code == 404

# :: GET /customers/1 - Tenta buscar os registros de um cliente, passando um parametro valido e que existe, é esperado o cliente
def test_get_customer(client, headers):
    resp = client.get('/v1/customers/1', headers=headers)
    expected = {'id': 1, 'name': 'Paola Florencia Carosella', 'email': 'paola.carosella@masterchef.com'}
    assert resp.status_code == 200
    assert resp.json == expected

# :: PUT /customers/<int:id> - Tenta atualizar um cliente que não existe, é esperado um 404
def test_put_customer_doesnt_exists(client, headers):
    data = { 'name': 'Paola'}
    resp = client.put('/v1/customers/999', json=data, headers=headers)
    assert resp.status_code == 404
    assert resp.json['message'] == 'Você está buscando um registro que não existe'

# :: PUT /customers/<int:id> - Tenta atualizar um cliente, passando um parametro errado, é esprado um 404
def test_put_customer_invalid_parameters(client, headers):
    data = { 'name': 'Paola'}
    resp = client.put('/v1/customers/AAA', json=data, headers=headers)
    assert resp.status_code == 404

# :: PUT /customers/<int:id> - Atualiza o nome de um cliente, é esperado que retorne 200 e o novo objeto
def test_put_customer(client, headers): 
    data = { 'name': 'Paola'}
    expected = {'id': 1, 'name': 'Paola', 'email': 'paola.carosella@masterchef.com'}
    resp = client.put('/v1/customers/1', json=data, headers=headers)
    assert resp.status_code == 200
    assert resp.json ==  expected

# :: DEL /customers/<int:id> - Tenta remover um registro que não existe, é esperado um 404
def test_delete_customer_doesnt_exists(client, headers):
    resp = client.delete('/v1/customers/999', headers=headers)
    assert resp.status_code == 404
    assert resp.json['message'] == 'Você está buscando um registro que não existe'

# :: DEL /customers/<int:id> - Tenta remover um registro, passando parametros invalidos
def test_delete_customer_invalid_parameter(client, headers):
    resp = client.delete('/v1/customers/999', headers=headers)
    assert resp.status_code == 404

# :: DEL /customers/<int:id> - Remove o ultimo client adicionado
def test_delete_customer(client, headers):
    resp = client.delete('v1/customers/4', headers=headers)
    assert resp.status_code == 204

# :: GET /<int:id>/wishlist - Tenta buscar a wishlist de um usuário que não existe
def test_get_customer_doesnt_exists_wishlist(client, headers):
    resp = client.get('/v1/customers/999/wishlist', headers=headers)
    assert resp.status_code == 200
    assert resp.json['data'] == []

# :: GET /<int:id>/wishlist - Tenta buscar a wishlist passando um parametro invalido
def test_get_customer_invalid_parameter_wishlist(client, headers):
    resp = client.get('/v1/customers/1/wishlist', headers=headers)
    expected = [{
        'id' : '1bf0f365-fbdd-4e21-9786-da459d78dd1f',
        'title' : 'Cadeira para Auto Iseos Bébé Confort Earth Brown',
        'image' : 'http://challenge-api.luizalabs.com/images/1bf0f365-fbdd-4e21-9786-da459d78dd1f.jpg',
        'price' : 1699.0
        }, {
        'id': '6a512e6c-6627-d286-5d18-583558359ab6',
        'title' : 'Moisés Dorel Windoo 1529',
        'image' : 'http://challenge-api.luizalabs.com/images/6a512e6c-6627-d286-5d18-583558359ab6.jpg',
        'price': 1149.0}, {
        'id': '212d0f07-8f56-0708-971c-41ee78aadf2b',
        'title': 'The Walking Dead - Game of the Year Edition',
        'image': 'http://challenge-api.luizalabs.com/images/212d0f07-8f56-0708-971c-41ee78aadf2b.jpg',
        'price': 149.9
    }]
    assert resp.status_code == 200
    assert resp.json['data'] == expected

# :: POST /<int:id>/wishlist - Tenta adicionar um produto com o tipo de dado invalido
def test_post_product_wishlist_invalid_parameter(client, headers):
    data = { 'product_id': 123 }
    resp = client.post('/v1/customers/1/wishlist', json=data, headers=headers)
    assert resp.status_code == 400
    assert resp.json['errors']['product_id'] == "123 is not of type 'string'"

# :: POST /<int:id>/wishlist - Tenta adicionar um produto com um ID de produto que não existe, é esperado um PRODUCT_NOT_FOUND
def test_post_product_wishlist_invalid_parameter(client, headers):
    data = { 'product_id': '123' }
    resp = client.post('/v1/customers/1/wishlist', json=data, headers=headers)
    assert resp.status_code == 404
    assert resp.json['message'] == 'Esse produto não existe'

# :: POST /<int:id>/wishlist - Adiciona um produto valido a um cliente
def test_post_product_wishlist(client, headers):
    data = { 'product_id': '571fa8cc-2ee7-5ab4-b388-06d55fd8ab2f' }
    resp = client.post('/v1/customers/1/wishlist', json=data, headers=headers)
    expected = {
        "id": "571fa8cc-2ee7-5ab4-b388-06d55fd8ab2f",
        "title": "Churrasqueira Elétrica Mondial 1800W",
        "image": "http://challenge-api.luizalabs.com/images/571fa8cc-2ee7-5ab4-b388-06d55fd8ab2f.jpg",
        "price": 159.0,
        "review_score": 4.352941
    }
    assert resp.status_code == 201
    assert resp.json == expected

# :: POST /<int:id>/wishlist - Tenta adicionar um produto que já existe
def test_post_product_wishlist_duplicate_entry(client, headers):
    data = { 'product_id': '571fa8cc-2ee7-5ab4-b388-06d55fd8ab2f' }
    resp = client.post('/v1/customers/1/wishlist', json=data, headers=headers)
    assert resp.status_code == 409
    assert resp.json['message'] == 'Você está tentando inserir um registro que já existe'

# :: GET /<int:id>/wishlist/<string:product_id> - Busca um produto unico da wishlist do cliente
def test_get_product_wishlist_from_client(client, headers):
    resp = client.get('/v1/customers/1/wishlist/1bf0f365-fbdd-4e21-9786-da459d78dd1f', headers=headers)
    expected = {
        "id": "1bf0f365-fbdd-4e21-9786-da459d78dd1f",
        "title": "Cadeira para Auto Iseos Bébé Confort Earth Brown",
        "image": "http://challenge-api.luizalabs.com/images/1bf0f365-fbdd-4e21-9786-da459d78dd1f.jpg",
        "price": 1699.0
    }
    assert resp.status_code == 200
    assert resp.json == expected

# :: DEL /<int:id>/wishlist/<string:product_id> - Deleta um produto da lista
def test_del_product_doesnt_exists_from_wishlist(client, headers):
    resp = client.delete('/v1/customers/1/wishlist/123', headers=headers)
    assert resp.status_code == 404
    assert resp.json['message'] == 'Você está buscando um registro que não existe'

# :: DEL /<int:id>/wishlist/<string:product_id> - Deleta um produto da lista
def test_del_product_from_wishlist(client, headers):
    resp = client.delete('/v1/customers/1/wishlist/571fa8cc-2ee7-5ab4-b388-06d55fd8ab2f', headers=headers)
    assert resp.status_code == 204
