
"""
    Chamadas referentes aos ENDPOINTS que estão no core
"""

def test_core_health(client):
    response = client.get('/health')
    assert response.status_code == 200
    assert response.json['message'] == 'I\'m alive and dancing smooth criminal!'

def test_core_root(client):
    response = client.get('/')
    assert response.status_code == 302
    assert response.headers['Content-Type'] == 'text/html; charset=utf-8'
