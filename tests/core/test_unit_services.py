"""
    Testes dos métodos genericos que está no service do core
"""

import pytest
from app.contrib.errors import CustomError, NOT_FOUND, DUPLICATE_ENTRY, INVALID_PARAMETERS

# :: Vamos usar o customer de exemplo, para testar os metodos o core
from app.v1.customers.models import Customer
from app.core.services import Service

class TestService(Service):
    __model__ = Customer

core_service = TestService()

# :: Método get_or_404, Caso passe um id que não existe no banco é esperado o retorno 404 NOT_FOUND
def test_get_or_404_with_number():
    with pytest.raises(CustomError) as ex:
        resp = core_service.get_or_404(999)
    assert ex._excinfo[1] == NOT_FOUND

# :: Método get_or_404, Caso passe um valor que não é numérico é esperado o retorno 404 NOT_FOUND
def test_get_or_404_with_letter():
    with pytest.raises(CustomError) as ex:
        resp = core_service.get_or_404('AAA')
    assert ex._excinfo[1] == NOT_FOUND

# :: Método get_404, passando valor para conseguir retornar
def test_get_or_404():
    resp = core_service.get_or_404(1)
    expected = Customer(id = 1, name = 'Paola Carosella', email = 'paola.carosella@masterchef.com')
    assert resp.id == expected.id
    assert resp.name == expected.name
    assert resp.email == expected.email

# :: Método first, Caso passe um ID que não tem no banco é esperado um None 
def test_first_non_exists():
    resp = core_service.first(id=999)
    assert resp == None

# :: Método first, Caso passe um ID que existe, é esperado apenas o objeto único
def test_first_exists():
    expected = Customer(id = 1, name = 'Paola Carosella', email = 'paola.carosella@masterchef.com')
    resp = core_service.first(id=1)
    assert resp.id == expected.id
    assert resp.name == expected.name
    assert resp.email == expected.email

# :: Método find, Caso passe um parametro que não existe para buscar, é esperado um array vazio
def test_find_non_exists():
    resp = core_service.find(id=999).all()
    assert len(resp) == 0

# :: Método find, Caso passe um parametro que existe, é esperado um array com um/ou objeto/objetos dentro
def test_find_exists():
    resp = core_service.find(id=1).all()
    assert len(resp) > 0

# :: Método get_all, Caso passe um parametro que não existe, é esperado um array vazio
def test_get_all_with_id_no_exists():
    resp = core_service.get_all(999, 998)
    assert len(resp) == 0

# :: Método get_all, Caso passe um parametro que existe, é esperado um array com os objetos dentro
    resp = core_service.get_all(2, 3)
    assert len(resp) == 2

# :: Método create, Caso passe uma entidade com parametros certos e que não exista, é esperado que insira normalmente
def test_create():
    resp = core_service.create(name='Lucas Lima', email='lucas.carvalho@luizalabs.com')
    assert resp.id == 4
    assert resp.name == 'Lucas Lima'
    assert resp.email == 'lucas.carvalho@luizalabs.com'

# :: Método create, Caso passe um registro que já exista, é esperado um IntegrityError
def test_create_duplicate_entry():
    with pytest.raises(CustomError) as ex:
        resp = core_service.create(name='Paola Carosella', email='paola.carosella@masterchef.com')
    assert ex._excinfo[1] == DUPLICATE_ENTRY

# :: Método delete, Caso passe um id que não existe, é esperado explodir um erro, pois não é possivel deleter um objeto que não tenha vindo do banco de dados
def test_delete_with_wrong_parameter():
    with pytest.raises(Exception) as ex:
        customer = Customer(id = 999, name = 'Teste Delete', email = 'teste.delete@teste.com')
        resp = core_service.delete(customer)
    
# :: Método delete, passando um objeto que existe no banco, é esperado deletar normalmente
def test_delete():
    customer = core_service.first(id=4)
    resp = core_service.delete(customer)

# :: Método update, passando apara atualizar o nome de uma entidade, o esperado é que retorne o objeto atualizado
def test_update():
    customer = core_service.first(id=1)
    resp = core_service.update(customer, name="Paola Florencia Carosella")
    expected = Customer(id = 1, name = 'Paola Florencia Carosella', email = 'paola.carosella@masterchef.com')
    assert resp.id == expected.id
    assert resp.name == expected.name
    assert resp.email == expected.email

# :: Método update, Caso passe uma model que não existe no banco, é esperado explodir um erro, pois não é possivel deleter um objeto que não tenha vindo do banco de dados
def test_update_with_wrong_parameter():
    with pytest.raises(Exception) as ex:
        customer = Customer(id = 999, name = 'Teste Delete', email = 'teste.delete@teste.com')
        resp = core_service.delete(customer)

# :: Método page, passando parametros certos, para ver a paginação funcionando
def test_page():
    args = { 'limit': 1, 'offset': 2, 'order': None }
    resp = core_service.page(args)
    
    assert resp.per_page == args['limit']
    assert resp.page == args['offset']

# :: Método page, filtrando pelo nome e com use_like=True
def test_page_filter_by_name():
    args = { 'limit': 1, 'offset': 1, 'order': None, 'name': 'Érick Jacquin' }
    resp = core_service.page(args, use_like=False)
    
    assert resp.per_page == args['limit']
    assert resp.page == args['offset']
    assert resp.items[0].name == 'Érick Jacquin'

# :: Método page, filtrando pelo nome e com use_like=True
def test_page_filter_by_name_and_use_like():
    args = { 'limit': 1, 'offset': 1, 'order': None, 'name': 'Jacquin' }
    resp = core_service.page(args)
    
    assert resp.per_page == args['limit']
    assert resp.page == args['offset']
    assert resp.items[0].name == 'Érick Jacquin'

# :: Método page, filtrando pelo nome e pedindo para retornar a query
def test_page_filter_by_name_and_request_query():
    args = { 'limit': 1, 'offset': 1, 'order': None, 'name': 'Érick Jacquin' }
    resp = core_service.page(args, return_query=True)
    assert 'SELECT customers.id AS customers_id' in str(resp)

# :: Método page, não passando o parametro offset
def test_page_without_limit():
    args = { 'offset': 1, 'order': None }
    resp = core_service.page(args)
    assert resp.per_page == 20 # valor padrão, per_page é o mesmo que limit
    assert resp.page == args['offset'] # page é o mesmo que offset

# :: Método all, testa trazer os primeiros registros da tabela, sem filtro algum, é basicamente um select * from
def test_all():
    resp = core_service.all()
    assert len(resp) == 3

# :: Método _isinstance, serve para verificar que é a model correta que está sendo passada ao service
def test_isinstance_with_another_instance():
    with pytest.raises(Exception) as ex:
        resp = core_service._isinstance(Customer)
        assert resp == ValueError

# :: Método _isinstance, passando a mesma instancia, é esperado que de certo
def test_isinstance():
    customer = core_service.first(id=1)
    resp = core_service._isinstance(customer)
