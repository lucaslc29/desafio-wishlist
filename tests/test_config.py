import os
from app.config import config

"""
    Testa se as configurações do ambiente de teste estão corretas
"""

def test_testing_config(app):
    app.config.from_object(config['testing'])
    assert app.config['TESTING']
    assert app.config['SQLALCHEMY_DATABASE_URI'] == 'sqlite://'
    assert not app.config['PRESERVE_CONTEXT_ON_EXCEPTION']
    assert not app.config['SQLALCHEMY_TRACK_MODIFICATIONS']
    assert app.config['SERVER_NAME'] == 'localhost:5000'
