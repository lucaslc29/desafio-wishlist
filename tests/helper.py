def create_header_with_authorization(client):
    resp = client.post('/auth', json={ 'username': 'luizalabs', 'password': 'luizalabs$1' })
    token = resp.json['access_token']
    headers = {
        'Authorization': f'jwt {token}',
        'Content-Type': 'application/json',
    }
    return headers
