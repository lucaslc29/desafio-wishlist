from app.contrib.routes_help import create_page_result, paginate

# :: mock do objeto pagination
class Pagination():
    def __init__(self):
        self.page = 1
        self.per_page = 20
        self.total = 5
        self.items = [
            {"id": 1, "name": "Paola Carosella", "email": "paola.carosella@masterchef.com"},
            { "id": 2, "name": "Henrique Fogaça", "email": "henrique.fogaca@masterchef.com"}
        ]
        self.has_prev = 'http://localhost:5000/v1/customers/?limit=20&offset=0'
        self.has_next = 'http://localhost:5000/v1/customers/?limit=20&offset=0'
        self.prev_num = 0
        self.next_num = 0

pagination = Pagination()
args = { 'limit': 20, 'offset': 1, 'order': None }

# :: Método create_page_result, formata o retorno das apis
def test_create_page_result():
    resp = create_page_result('v1.customers_customer_list', pagination, args)
    expected_meta = {
        'prev': 'http://localhost:5000/v1/customers/?limit=20&offset=0',
        'next': 'http://localhost:5000/v1/customers/?limit=20&offset=0',
        'limit': 20,
        'offset': 1,
        'order': None,
        'total': 5
    }
    
    assert resp['meta'] == expected_meta
    assert resp['data'] == pagination.items

# :: Método paginate, usado para fazer as paginações do resultado
def test_paginate():

    @paginate
    def my_mock(args):
        print('args')
        print(args)
        return args

    assert callable(my_mock)