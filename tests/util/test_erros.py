from app.contrib.errors import CustomError, handle_custom_error, handle_any_error_as_custom_error
custom_error = CustomError()

# :: Método to_json, transforma o erro em um json
def test_custom_error_to_json():    
    custom_error.code = 1
    custom_error.message = 'Bad Request'
    custom_error.status = 400

    resp = custom_error.to_json()
    expected = {
        'status': 400,
        'code': 1,
        'message': 'Bad Request'
    }
    assert resp == expected

# :: Método handle_custom_error
def test_handle_custom_error():
    resp = handle_custom_error(custom_error)
    assert resp[1] == 400

# :: Método handle_any_error_as_custom_error
def test_handle_any_error_as_custom_error():
    resp = handle_any_error_as_custom_error(Exception('Algo Aconteceu !'))
    assert resp[1] == 500