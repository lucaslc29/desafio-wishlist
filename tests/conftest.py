import pytest

from random import randint
from datetime import datetime, timedelta
from app.contrib.errors import register_custom_error_handler
from app.v1.customers.models import Customer
from app.v1.customers_products.models import CustomerProduct
from app import create_app, db
from app.contrib.errors import handle_any_error_as_custom_error

from app.core.services import Service

@pytest.fixture(scope='session')
def app():
    app = create_app('testing')
    with app.app_context():
        db.create_all()
        load_data()
        register_custom_error_handler(app)
        yield app
        db.drop_all()

def load_data():
    # :: Adiciona clientes
    db.session.add(Customer(name='Paola Carosella', email='paola.carosella@masterchef.com'))
    db.session.add(Customer(name='Henrique Fogaça', email='henrique.fogaca@masterchef.com'))
    db.session.add(Customer(name='Érick Jacquin', email='erick.jacquin@masterchef.com'))

    # :: Adiciona produtos na wishlist do primeiro cliente
    db.session.add(CustomerProduct(customer_id=1, product_id='1bf0f365-fbdd-4e21-9786-da459d78dd1f'))
    db.session.add(CustomerProduct(customer_id=1, product_id='6a512e6c-6627-d286-5d18-583558359ab6'))
    db.session.add(CustomerProduct(customer_id=1, product_id='212d0f07-8f56-0708-971c-41ee78aadf2b'))

    db.session.commit()
